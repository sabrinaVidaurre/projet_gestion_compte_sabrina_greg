<?php
// mes variables
$tabAgences = [["codeAgence"=>111, "nomAgence"=> "Banque DWWM", "adresseAgence" => "10 rue de la fortune 59000 lille"]];
$tabCB = [];
$tabClients = [["identifiant"=>"AZ000002", "nom"=> "lou", "prenom" => "lea" , "dateNais" => "11/11/2020", "email" => "lou@gmail.fr" ]];
$cpteurClient = 1;
$cpteurClient = str_pad($cpteurClient, 6, "0", STR_PAD_LEFT);
$cpteurAgence = 1;
$cpteurAgence = str_pad($cpteurAgence, 3, "0", STR_PAD_LEFT);
$cpteurCpt = 1;
$cpteurCpt = str_pad($cpteurCpt, 5, "0", STR_PAD_LEFT);


// mes functions
include ('functions.php');

echo (PHP_EOL);
echo (PHP_EOL);

echo ("         *************   Bienvenue à la DWWM_20044 BANQUE!   **********".PHP_EOL);

while (true) {
    
    include ('menu_banque.php');

    if ((preg_match("#^[0-9]+$#", $choix))){  //pour filter les chiffres (sans caractères)  
       
        if ($choix=="1") { //créer une agence
            $Agence= [];
            creaAgence ($tabAgences, 0, $cpteurAgence);
            $cpteurAgence = str_pad($cpteurAgence+1, 3, "0", STR_PAD_LEFT);

        }
        elseif ($choix=="2") {  // créer un client

            echo("                       ----- Créer un client -----" .PHP_EOL);

           
                $nom = strtolower(readline ("Veuillez renseigner le nom du client : "));
                while (!(preg_match('#^[a-z ]{1,}$#', $nom))){
                    echo "---------> Votre saisie n'est pas valide : ... Veuillez saisir en lettres ".PHP_EOL;
                    echo (PHP_EOL);
                    $nom = strtolower(readline ("Veuillez renseigner le nom du client : "));   
                }
                $prenom = strtolower(readline ("Veuillez renseigner le prénom du client : "));
                while (!(preg_match('#^[a-z ]+$#', $prenom))){
                    echo "---------> Votre saisie n'est pas valide : ... Veuillez saisir en lettres ".PHP_EOL;
                    echo (PHP_EOL);
                    $prenom = strtolower(readline ("Veuillez renseigner le prénom du client : "));
                }
                $jour =  readline ("Veuillez saisir le jour de naissance du client (jj) : ");
                while (!(preg_match('#^[0-3][0-9]+$#', $jour))){
                    echo "---------> Votre saisie n'est pas valide : ... Veuillez saisir format 00 ".PHP_EOL;
                    echo (PHP_EOL);
                    $jour =  readline ("Veuillez saisir le jour de naissance du client (jj) : ");
                }
                $mois = readline ("Veuillez saisir le mois de naissance du client (mm) : ");
                while (!(preg_match('#^[0-1][0-9]+$#', $mois))){
                    echo "---------> Votre saisie n'est pas valide : ... Veuillez saisir au format 00 ".PHP_EOL;
                    echo (PHP_EOL);
                    $mois = readline ("Veuillez saisir le mois de naissance du client (mm) : ");
                }
                $annee = readline ("Veuillez saisir l'annee de naissance du client (aaaa) : ");
                while (!(preg_match('#^[1-9]{4}$#', $annee))){
                    echo "---------> Votre saisie n'est pas valide : ... Veuillez saisir au format 0000  ".PHP_EOL;
                    echo (PHP_EOL);
                    $annee = readline ("Veuillez saisir l'annee de naissance du client (aaaa) : ");
                }
                $date = ($jour."/".$mois."/".$annee);
                $email = readline ("Veuillez renseigner l'e-mail du client : ");
                while (!(filter_var($email, FILTER_VALIDATE_EMAIL))){
                    echo "---------> Votre saisie n'est pas valide : ... Veuillez saisir au format aze@rty.fr  ".PHP_EOL;
                    echo (PHP_EOL);
                    $email = readline ("Veuillez renseigner l'e-mail du client : ");
                }
            
                if ((filter_var($email, FILTER_VALIDATE_EMAIL))){
            
                    $cpt = 0;   
                    for ($i=0; $i<count($tabClients); $i++) { 
                        if (($tabClients[$i]["nom"] == $nom)&&($tabClients[$i]["prenom"] == $prenom)&&($tabClients[$i]["dateNais"] == $date)) { 
                            echo " ---->> Ce client existe déja dans notre base de donnée !!";
                            echo (PHP_EOL);
                            break;
                            } 
                        else {
                            $cpt++;
                        }
                    }
                   
                    if ($cpt == count($tabClients)) { 
                        echo (PHP_EOL);
                       
                        $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            
                        $client["identifiant"]= ($alphabet[rand(0, strlen($alphabet))].$alphabet[rand(0, strlen($alphabet))].$cpteurClient);
                        $client["nom"] = $nom;
                        $client["prenom"] = $prenom;
                        $client["dateNais"] = $date; 
                        $client["email"] = $email;
                    
                        $tabClients [] = $client; //entrer le tab client dans le tab tabclients 
                        echo ("~~~~ Votre saisie est enregistée ~~~~ ".PHP_EOL);
                      
                        affichageClient ($client);
                        
                        // var_dump ($tabClients);
                        $cpteurClient = str_pad($cpteurClient+1,6, "0", STR_PAD_LEFT);
                    }
                }
                else {
                    echo ("---------> Mauvaise adresse mail <----------");
                }
            
        }
        elseif ($choix=="3") { // créer compte bancaire

            echo("                         ----- Créer un compte bancaire -----" .PHP_EOL);

                  
            $numIdentifiant = strtolower(readline ("Avez-vous un identifiant client (o/n) : "));

            if (preg_match("#^[n]+$#", $numIdentifiant)&&!(empty($numIdentifiant))) {
                echo "Veuillez tout d'abord créer un compte client ...".PHP_EOL;
                 // fonction a travailler
            }
            elseif (preg_match("#^[o]+$#", $numIdentifiant)) {
                
                $identifiant = strtoupper(readline("Veuillez saisir l'identifiant du client : "));
                $cptClient = false;

                while (!(preg_match("#^[A-Z]{2}[0-9]{6}+$#", $identifiant)&&!(empty($identifiant)))) {
                    echo ("Saisie invalide". PHP_EOL);
                    $identifiant = strtoupper(readline(" Veuillez saisir un identifiant client : " . PHP_EOL));
                }

                foreach ($tabClients as $val1) { //parcours de tableau pour trouver les clients existants
          
                     if ($val1["identifiant"] == $identifiant) {
                        $cptClient = true;    
                        break;               
                        }     
                }
               
                if (!$cptClient) {
                   echo "Ce client n'est pas enregistré dans la base de données";
                }
                else{

                    if (preg_match("#^[A-Z]{2}[0-9]{6}+$#", $identifiant)) {
                        $compteexistant = [];
                        $cptExist= 0;
                        foreach ($tabCB as $compte) { //parcours de tableau pour trouver les comptes existants
                      
                            foreach ($compte as $cle => $valeur) {
                           
                                if ($valeur == $identifiant) { 
                                    $cle = strtoupper($compte ["type"]);
                                    $valeur = $compte ["solde"];
                                    $compteexistant []= [$cle=>$valeur];
                                  
                                    echo  "Le client détient déja un compte ".$compte ["type"].PHP_EOL ;    
                                    break;
                                } 
                                else {
                                   $cptExist++;
                                }                        
                            }
                        } 
                        
                                        
                        if (count ($compteexistant)==3) {
                            echo "Le client détient déja les 3 types de comptes : création impossible".PHP_EOL;
                            echo (PHP_EOL);
                        break;
                         }
                        
                        elseif ($cptExist == 0) {
                                 echo "Le client n'a pour l'instant aucun compte de créé".PHP_EOL;
                                 echo (PHP_EOL);
                             
                        }
                        //var_dump ($tabCB);
                            echo (PHP_EOL);
                            echo ("Veuillez renseigner le type de compte souhaité".PHP_EOL);
                            echo (PHP_EOL);
                            $typeCompte = strtoupper (readline  ("Livret A => 1, Compte courant => 2 ou PEL => 3 : "));
                            
                            while (!(preg_match('#^[0-3]{1}$#', $typeCompte))) {// ne fonctionne pas à revoir
                                echo "---------> La saisie est invalide".PHP_EOL;
                                $typeCompte = strtoupper (readline  ("Livret A => 1, Compte courant => 2 ou PEL => 3 : "));
                            }  
                            echo (PHP_EOL);
    
                            if (preg_match("#^[1]$#", $typeCompte)) {
                               
                                $typeCompte = "LIVRET A";         
                                
                                $cpt = true;
                              
                                foreach ($compteexistant as $index) {  // vérif de l'existence du compte fonctionne plus
                                     foreach ($index as $key => $value) {
                                        if ($key == $typeCompte) {
                                            $cpt = false;
                                        }
                                     }                          
                                            
                                }                                
                                if (!$cpt) {
                                    
                                    echo "---------> Saisie invalide, le client détient déja un ".$typeCompte.PHP_EOL;
                                    echo (PHP_EOL);
                                
                                }
                                else {        
                                           
                                    $codeAgence =  readline ("Veuillez saisir le code agence (3 chiffres) : ");
                                    echo (PHP_EOL);
                                    
                                    while (!(preg_match('#^[0-9]{3}$#', $codeAgence))){
                                        echo "---------> Votre saisie n'est pas valide : ... code erroné (format : 000) ".PHP_EOL;
                                        echo (PHP_EOL);
                                        $codeAgence = readline ("Veuillez saisir le code agence (3 chiffres) : ");
                                        
                                    }
    
                                    agenceExiste($tabAgences,$codeAgence,$codeAgence);

                                                                           
                                    $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
    
                                    while (!is_numeric($solde)) {// ne fonctionne pas à revoir
                                        echo "---------> Le solde n'est pas un numérique".PHP_EOL;
                                        $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
                                    } 
    
                                    $decouvert = strtolower(readline("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL));
                                    
                                    while (($decouvert != "o")&&($decouvert !="n")){
                                        echo "---------> Votre saisie n'est pas valide".PHP_EOL;
                                        $decouvert = readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL);
                                    } 
    
                                    $decouvert == "o" ? $decouvert = "oui" : $decouvert = "non";
    
                                    // insertion des données
                                    $numCompte= $cpteurCpt.date('his');
                                    $compte["Ncompte"]= $numCompte;
                                    $compte["identifiant"] = strtoupper($identifiant);
                                    $compte["codeAgence"] = $codeAgence;
                                    $compte["type"] = $typeCompte; 
                                    $compte["solde"] = $solde;
                                    $compte["decouvert"] = strtolower($decouvert);
                    
                                   
                                    $tabCB[]=$compte;
                                    echo PHP_EOL;
                                    echo ("~~~~~~~~ Votre saisie est enregistée ~~~~~~~~ ".PHP_EOL);
                                    
                                                                    
                                    affichageCompte($compte,$tabAgences, $codeAgence, $cpteurAgence);
                                    $cpteurCpt = str_pad($cpteurCpt+1, 5, "0", STR_PAD_LEFT);                                                       
                                    
                                }
                            }
                            if (preg_match("#^[2]$#", $typeCompte)) {
                               
                                $typeCompte = "CCP";         
                                
                                $cpt = true;
                               
                                foreach ($compteexistant as $index) {  // vérif de l'existence du compte fonctionne plus
                                     foreach ($index as $key => $value) {
                                        if ($key == $typeCompte) {
                                            $cpt = false;
                                        }
                                     }                          
                                            
                                }                                
                                if (!$cpt) {
                                    
                                    echo "---------> Saisie invalide, le client détient déja un ".$typeCompte.PHP_EOL;
                                    echo (PHP_EOL);
                                
                                }
                                else {        
                                           
                                    $codeAgence =  readline ("Veuillez saisir le code agence (3 chiffres) : ");
                                    echo (PHP_EOL);
                                    
                                    while (!(preg_match('#^[0-9]{3}$#', $codeAgence))){
                                        echo "---------> Votre saisie n'est pas valide : ... code erroné (format : 000) ".PHP_EOL;
                                        echo (PHP_EOL);
                                        $codeAgence = readline ("Veuillez saisir le code agence (3 chiffres) : ");
                                        
                                    }
    
                                    agenceExiste($tabAgences,$codeAgence,$codeAgence);

                                                                           
                                    $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
    
                                    while (!is_numeric($solde)) {// ne fonctionne pas à revoir
                                        echo "---------> Le solde n'est pas un numérique".PHP_EOL;
                                        $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
                                    } 
    
                                    $decouvert = strtolower(readline("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL));
                                    
                                    while (($decouvert != "o")&&($decouvert !="n")){
                                        echo "---------> Votre saisie n'est pas valide".PHP_EOL;
                                        $decouvert = readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL);
                                    } 
    
                                    $decouvert == "o" ? $decouvert = "oui" : $decouvert = "non";
    
                                    // insertion des données
                                    $numCompte= $cpteurCpt.date('his');
                                    $compte["Ncompte"]= $numCompte;
                                    $compte["identifiant"] = strtoupper($identifiant);
                                    $compte["codeAgence"] = $codeAgence;
                                    $compte["type"] = $typeCompte; 
                                    $compte["solde"] = $solde;
                                    $compte["decouvert"] = strtolower($decouvert);
                    
                                   
                                    $tabCB[]=$compte;
                                    echo PHP_EOL;
                                    echo ("~~~~~~~~ Votre saisie est enregistée ~~~~~~~~ ".PHP_EOL);
                                    
                                                                    
                                    affichageCompte($compte,$tabAgences, $codeAgence, $cpteurAgence);
                                    $cpteurCpt = str_pad($cpteurCpt+1, 5, "0", STR_PAD_LEFT);                                                       
                                    
                                }
                            }
                            if (preg_match("#^[3]$#", $typeCompte)) {
                               
                                $typeCompte = "PEL";         
                                
                                $cpt = true;
                              
                                foreach ($compteexistant as $index) {  // vérif de l'existence du compte fonctionne plus
                                     foreach ($index as $key => $value) {
                                        if ($key == $typeCompte) {
                                            $cpt = false;
                                        }
                                     }                          
                                            
                                }                                
                                if (!$cpt) {
                                    
                                    echo "---------> Saisie invalide, le client détient déja un ".$typeCompte.PHP_EOL;
                                    echo (PHP_EOL);
                                
                                }
                                else {        
                                           
                                    $codeAgence =  readline ("Veuillez saisir le code agence (3 chiffres) : ");
                                    echo (PHP_EOL);
                                    
                                    while (!(preg_match('#^[0-9]{3}$#', $codeAgence))){
                                        echo "---------> Votre saisie n'est pas valide : ... code erroné (format : 000) ".PHP_EOL;
                                        echo (PHP_EOL);
                                        $codeAgence = readline ("Veuillez saisir le code agence (3 chiffres) : ");
                                        
                                    }
    
                                    agenceExiste($tabAgences,$codeAgence,$codeAgence);

                                                                           
                                    $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
    
                                    while (!is_numeric($solde)) {// ne fonctionne pas à revoir
                                        echo "---------> Le solde n'est pas un numérique".PHP_EOL;
                                        $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
                                    } 
    
                                    $decouvert = strtolower(readline("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL));
                                    
                                    while (($decouvert != "o")&&($decouvert !="n")){
                                        echo "---------> Votre saisie n'est pas valide".PHP_EOL;
                                        $decouvert = readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL);
                                    } 
    
                                    $decouvert == "o" ? $decouvert = "oui" : $decouvert = "non";
    
                                    // insertion des données
                                    $numCompte= $cpteurCpt.date('his');
                                    $compte["Ncompte"]= $numCompte;
                                    $compte["identifiant"] = strtoupper($identifiant);
                                    $compte["codeAgence"] = $codeAgence;
                                    $compte["type"] = $typeCompte; 
                                    $compte["solde"] = $solde;
                                    $compte["decouvert"] = strtolower($decouvert);
                    
                                   
                                    $tabCB[]=$compte;
                                    echo PHP_EOL;
                                    echo ("~~~~~~~~ Votre saisie est enregistée ~~~~~~~~ ".PHP_EOL);
                                    
                                                                    
                                    affichageCompte($compte,$tabAgences, $codeAgence, $cpteurAgence);
                                    $cpteurCpt = str_pad($cpteurCpt+1, 5, "0", STR_PAD_LEFT);                                                       
                                    
                                }
                            }
  
                    }
                    else 
                    echo "---------> Saisie incorrect";
                }

            }
            
            else {
                echo "---------> Saisie incorrecte";
            }
        }
        elseif ($choix=="4") {  //recherche de compte

            echo("                         ----- Recherche de compte -----" .PHP_EOL);
			echo (PHP_EOL);
			
            $recherche_numero_compte = readline(" Veuillez saisir un numéro de compte : " . PHP_EOL);
            
            while (!(preg_match("#^[0-9]{11}+$#", $recherche_numero_compte))) {
                   echo ("Saisie invalide".PHP_EOL);
                   $recherche_numero_compte = readline(" Veuillez saisir un numéro de compte : " . PHP_EOL);
            }
			$id_personne_chercher = NULL;

			foreach ($tabCB as $num_compte_en_cours) {
	
				if ($num_compte_en_cours["Ncompte"] === $recherche_numero_compte) {
		
                $id_personne_chercher = $num_compte_en_cours ["identifiant"];
                $codeAgence1 = $num_compte_en_cours ["codeAgence"];
                $lesolde = $num_compte_en_cours ["solde"];
                $ledecouvert = $num_compte_en_cours ["decouvert"];
                $letype = $num_compte_en_cours ["type"];
				
				
				}	
			}

				if ($id_personne_chercher == NULL) {
				echo ("ce compte n'existe pas");
				
				} 

				else {
		
					foreach ($tabClients as $client_en_cours) {
			
						if ($client_en_cours["identifiant"] === $id_personne_chercher ) {
			
						echo("Client : ".PHP_EOL);
						echo (PHP_EOL);
                        echo(" Code agence : ".$letype.PHP_EOL);
						echo (PHP_EOL);
                        
						echo(" Code agence : ".$codeAgence1.PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Identifiant : ".$client_en_cours["identifiant"].PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Nom : ".$client_en_cours["nom"].PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Prénom : ".$client_en_cours["prenom"].PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Solde : ".$lesolde.PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Découvert autorisé: ".$ledecouvert.PHP_EOL);
						echo (PHP_EOL);
           
						}
					}
				}
			
        }   
        elseif ($choix=="5") { // recherche de client

            echo("                         ----- Recherche de client -----" .PHP_EOL);
            echo (PHP_EOL);
			
			while (true) {

                echo (PHP_EOL);
                echo("1) Recherche par nom" .PHP_EOL);
                echo (PHP_EOL);
                
                echo("2) Recherche par n° de compte" .PHP_EOL);
                echo (PHP_EOL);
                
                echo("3) Recherche par identifiant client" .PHP_EOL);
                echo (PHP_EOL);
                
                echo("4) Retour au menu principal" .PHP_EOL);
                echo (PHP_EOL);
                
                
                $choix_recherche = readline(" Veuillez saisir un mode de recherche : " .PHP_EOL);
                echo (PHP_EOL);		
                
                if ((preg_match("#^[0-4]+$#", $choix_recherche))){
                    if ($choix_recherche=="1") {
                        
                        //function recherche($recherche_1, $tabClients, [nom]) {}
                        
                        echo("                         ----- Recherche par nom -----" .PHP_EOL);
                        echo (PHP_EOL);
                        
                        $recherche_nom = strtolower(readline(" Veuillez saisir un nom : " . PHP_EOL));
                        echo (PHP_EOL);
                        // var_dump($tabClients);
                        while (!(preg_match('#^[a-z ]{1,}$#', $recherche_nom))){
                        echo ("saisie invalide");
                        $recherche_nom = strtolower(readline(" Veuillez saisir un nom : " . PHP_EOL));
                        }
                        foreach ($tabClients as $key => $client) {
                            // var_dump($key);
                            if ($key === "nom" && $recherche_nom === $client) {
                        
                                foreach ($tabClients as $key => $value) {
                            
                                echo "$key : $value".PHP_EOL;
                                echo (PHP_EOL);
                                }
                            }
                            
                            elseif ($key === "nom" && $recherche_nom != $client) {
                        
                                    echo("Aucune correspondance trouvée.");
                                    echo (PHP_EOL);
                            }
                        }
                    }	
                        
                    if ($choix_recherche=="2") {
                        
                        //function recherche($recherche_2, $tabCb, [num_compte]) {}
                            
                        echo("                         ----- Recherche par n° de compte -----" .PHP_EOL);
                        echo (PHP_EOL);
                        
                        $recherche_numero_compte = readline(" Veuillez saisir un numéro de compte : " . PHP_EOL);
                        while (!(preg_match("#^[0-9]{11}+$#", $recherche_numero_compte))) {
                            echo ("Saisie invalide");
                            $recherche_numero_compte = readline(" Veuillez saisir un numéro de compte : " . PHP_EOL);
                            }

                        $id_personne_chercher = NULL;

                        foreach ($tabCB as $num_compte_en_cours) {
                    
                            if ($num_compte_en_cours["Ncompte"] === "$recherche_numero_compte") {
                        
                        $id_personne_chercher = $num_compte_en_cours ["identifiant"];
                        break;
                        
                            }	
                        }

                            if ($id_personne_chercher == NULL) {
                            echo ("Aucune correspondance trouvée");
                    
                            } 

                            else {
                        
                                foreach ($tabClients as $client_en_cours) {
                            
                                    if ($client_en_cours["identifiant"] === $id_personne_chercher ) {
                            
                                    echo("Client : ".PHP_EOL);
                                    echo (PHP_EOL);
                            
                                    echo(" Identifiant :".$client_en_cours["identifiant"].PHP_EOL);
                                    echo (PHP_EOL);
                            
                                    echo(" Nom :".$client_en_cours["nom"].PHP_EOL);
                                    echo (PHP_EOL);
                                    
                                    echo(" Prénom :".$client_en_cours["prenom"].PHP_EOL);
                                    echo (PHP_EOL);
                                    
                                    echo(" Date de naissance :".$client_en_cours["dateNais"].PHP_EOL);
                                    echo (PHP_EOL);
                                    
                                    echo(" E-mail :".$client_en_cours["email"].PHP_EOL);
                                    echo (PHP_EOL);
                                
                                    }
                            }
                            echo (PHP_EOL); 
                            }
                    }		
                    
                    if ($choix_recherche=="3") {
                    
                        //function recherche($recherche_3, $tabCb, [id_client]) {}
                            
                        echo("                         ----- Recherche par identifiant client -----" .PHP_EOL);
                        echo (PHP_EOL);
                                
                        $recherche_identifiant = strtoupper(readline(" Veuillez saisir un identifiant client : " . PHP_EOL));
                        echo (PHP_EOL);
                        while (!(preg_match("#^[A-Z]{2}[0-9]{6}+$#", $recherche_identifiant)&&!(empty($recherche_identifiant)))) {
                            echo ("Saisie invalide");
                            $recherche_identifiant = strtoupper(readline(" Veuillez saisir un identifiant client : " . PHP_EOL));
                        }

                        $cpt_id=0;
                        
                        foreach ($tabClients as $client) {
                    
                                foreach ($client as $key => $value) {
                                
                                    if ($key == "identifiant" && $recherche_identifiant == $value) {
                                                        
                                    echo ("Identifiant client : ".$client ["identifiant"]).PHP_EOL;
                                    echo ("Nom : ".$client ["nom"]).PHP_EOL;
                                    echo ("Prenom : ".$client ["prenom"]).PHP_EOL;
                                    echo ("Date de naissance : ".$client ["dateNais"]).PHP_EOL;
                                    echo ("E-mail : ".$client ["email"]).PHP_EOL;
                                            
                                    echo (PHP_EOL);
                                    }
                                    
                                    elseif ($key == "identifiant" && $recherche_identifiant !== $value)  {
                                        
                                    $cpt_id++ ;
                                    
                                    }
                                }	
                                    
                        }
                        if ($cpt_id == count ($tabClients))   {
                            echo("Aucune correspondance trouvée.");
                            echo (PHP_EOL);
                        }
                    }
                    
                    if ($choix_recherche=="4") {
                    
                        echo("                         ----- Retour au menu principal -----" .PHP_EOL);
                        echo (PHP_EOL);
                            
                            $retour= readline("Confirmez (o/n) : " .PHP_EOL);
                        
                                echo (PHP_EOL);
                        
                            if ($retour=="o") {
                                                                
                                break;
                            }
                    }	
                }
                else {
                    echo "saisie invalide";
                }
            }
            
        }
        elseif ($choix=="6") { //liste des comptes d'un client

                    echo("                         ----- Afficher la liste des comptes d’un client -----" .PHP_EOL);

                    $identifiant = strtoupper(readline("Veuillez saisir l'identifiant du client pour la consultation : "));

                    if (preg_match("#^[A-Z]{2}[0-9]{6}+$#", $identifiant)) {
                    
                        
                        foreach ($tabClients as $client) { //parcours de tableau pour trouver les clients existants
                    
                            foreach ($client as $cle => $valeur) {

                                if ($valeur == $identifiant) {
                                                
                                    echo PHP_EOL;
                                    echo ("~~~~ Liste des comptes du client ".$client["prenom"]." ".$client["nom"]." ~~~~ ".PHP_EOL);
                                    echo (PHP_EOL);
                                    affichageClient ($client);
                                }
                            }
                        }
                    
                        $cpt= 0;
                        echo ("~~~~~~~~~~ Comptes ~~~~~~~~~~~~".PHP_EOL);
                            foreach ($tabCB as $compte) {
                                foreach ($compte as $key => $value) {
                                    if ($value == $identifiant){
                                        echo (PHP_EOL);
                                        affichageCompte($compte, $tabAgences, $codeAgence, $cpteurAgence);
                                    }
                                    else{
                                    $cpt ++;
                                    }           
                                }
                            }
                        
                            if ($cpt == 0 ) {

                                echo "Le client n'a pour l'instant aucun compte de créé".PHP_EOL;
                            }
                    }
                    else
                        echo "-----------> Saisie invalide";                
                        
                       
        }
        elseif ($choix=="7") { // imprimer les infos client

            echo("                         ----- Imprimer les infos client -----" .PHP_EOL);

            $identifiant = strtoupper(readline("Veuillez saisir l'identifiant du client pour l'impression : "));

            if (preg_match("#^[A-Z]{2}[0-9]{6}+$#", $identifiant)) {
            
                
                foreach ($tabClients as $client) { //parcours de tableau pour trouver les clients existants
            
                    foreach ($client as $cle => $valeur) {

                        if ($valeur == $identifiant) {
                                        
                            echo PHP_EOL;
                            echo ("~~~~ Fiche du client ".$client["prenom"]." ".$client["nom"]." ~~~~ ".PHP_EOL);
                            echo (PHP_EOL);
                            affichageClient ($client);
                        }
                    }
                }
 
                $fichier = "infosClient.txt";
                $contenuIdent = ("~~~~ Fiche client : ".$client["prenom"]." ".$client["nom"]." ~~~~ ".PHP_EOL.PHP_EOL.PHP_EOL."N° identifiant :".$client["identifiant"]
                                .PHP_EOL."Nom : ".$client["nom"].PHP_EOL."prénom : ".$client["prenom"].PHP_EOL."Date de naissance : ".$client["dateNais"].PHP_EOL);                               
                $titre = PHP_EOL."__________________________________________________________________________________________________________________"
                .PHP_EOL."Liste de comptes ".PHP_EOL."__________________________________________________________________________________________________________________"
                .PHP_EOL."Numéro de comptes                                    Solde ".PHP_EOL."__________________________________________________________________________________________________________________".PHP_EOL;
                file_put_contents($fichier,$contenuIdent, FILE_USE_INCLUDE_PATH | LOCK_EX); // include path : Recherche le fichier filename dans le dossier d'inclusion. LOCK_EX pour empêcher quiconque d'autre d'écrire dans le fichier en même temps
                file_put_contents ($fichier, $titre ,FILE_APPEND | LOCK_EX);

                $cpt= 0;

                 echo ("~~~~~~~~~~ Comptes ~~~~~~~~~~~~".PHP_EOL);
                 foreach ($tabCB as $compte) {
                    foreach ($compte as $key => $value) {
                        if ($value == $identifiant){
                            echo (PHP_EOL);
                            affichageCompte($compte, $tabAgences, $codeAgence, $cpteurAgence);
                            $compte ["solde"] >= 0 ? $face = ":-)" : $face = ":-(";
                            $contenuCompte = $compte["Ncompte"]."                                         ".$compte["solde"]." euros                                 ".$face.PHP_EOL;
                            file_put_contents ($fichier, $contenuCompte ,FILE_APPEND| LOCK_EX);
                        }
                        else{
                        $cpt ++;
                        }           
                    }
                }
            
                if ($cpt == 0 ) {

                    echo "Le client n'a pour l'instant aucun compte de créé".PHP_EOL;
                    $contenuCompte = "Le client n'a pour l'instant aucun compte de créé".PHP_EOL;
                }
                     
                echo ("Transfert executé sur fichier .txt dans le dossier C:\EnvDev\projets_git\projet_gestion_compte_sabrina_greg");   
            } 
            else
            {
                echo "Saisie incorrecte";
            }
        }
        elseif ($choix=="8")  { // export fiche client

            $identifiant = strtoupper(readline("Veuillez saisir l'identifiant du client pour exportation : "));

            if (preg_match("#^[A-Z]{2}[0-9]{6}+$#", $identifiant)) {
            
                // $export= fopen ("exportclient.xls", "w");
                $export= fopen ("exportclient.csv", "w");
                foreach ($tabClients as $client) { //parcours de tableau pour trouver les clients existants
            
                    foreach ($client as $cle => $valeur) {

                        if ($valeur == $identifiant) {
                                        
                            $contenuIdent = ("~~~~ Fiche client : ".$client["prenom"]." ".$client["nom"]." ~~~~ ".PHP_EOL."Identifiant :".$client["identifiant"]
                            .PHP_EOL."Nom : ".$client["nom"].PHP_EOL."prenom : ".$client["prenom"].PHP_EOL."Date de naissance : ".$client["dateNais"].PHP_EOL);
                            fwrite ($export, $contenuIdent);
                        }
                    }
                }
               
                $cpt = 0;
                
                foreach ($tabCB as $compte) {
                    foreach ($compte as $key => $value) {
                        if ($value == $identifiant){
                            // $exportTab []= [$compte ["Ncompte"], $compte ["solde"], $compte ["decouvert"]];
                            $cpteur = 1;
                            
                            $contenuCompt = "Type : ".$compte["type"].PHP_EOL."Numero de compte : ".$compte ["Ncompte"].PHP_EOL. "Solde : ".$compte ["solde"].PHP_EOL. "decouvert autorise".$compte ["decouvert"].PHP_EOL;
                            fwrite ($export, $contenuCompt);
                                       
                            $contenuCompt .= $cpteur;
                        
                            $cpteur++;
                        }
                        else{
                        $cpt ++;
                        }           
                    }
                }
            
                if ($cpt == 0 ) {

                   fwrite($export, "Le client n'a pour l'instant aucun compte de créé".PHP_EOL ); 
                   }
                
                fclose ($export);



                echo "Export réalisé dans C:\EnvDev\projets_git\projet_gestion_compte_sabrina_greg";
            }
            else
            echo "saisie incorrecte".PHP_EOL;
            		
        }
        elseif ($choix=="9")  { //quitter

                    
                    break;
             		
        }
    }
    else {
        echo "  :(   Mmmhh, ... Saisie invalide   :(  ".PHP_EOL;
    }
}

?>