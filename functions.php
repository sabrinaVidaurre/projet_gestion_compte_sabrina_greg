<?php
declare (strict_types=1);

function agenceExiste(&$tabAgences, $codeAgence, $cpteurAgence){
    $cpt=0;
    for ($i=0; $i<count($tabAgences); $i++) {  //si code agence existe
        if ($tabAgences[$i]["codeAgence"] == $codeAgence){
           $nomAgence = $tabAgences[$i]["nomAgence"]; 
           return $nomAgence;
        }
        else
        {
          $cpt ++;  
        }
    }
    if ($cpt==count($tabAgences)) {
        echo ("Ce code agence n'existe pas... Veuillez procéder au préalable à sa création, menu 2");
        echo (PHP_EOL);
    }
}

function creaAgence (&$tabAgences, $codeAgence, $cpteurAgence){

    echo("                    ----- Créer une agence -----" .PHP_EOL);
         
    $cpt = 0;
    // var_dump ($tabAgences[0]["codeAgence"]);
   
    for ($i=0; $i<count($tabAgences); $i++) { 
        if ($tabAgences[$i]["codeAgence"] == $codeAgence) { 
            echo " ---->> Ce code existe déja !!";
            break;
            } 
        else {
            $cpt++;
        }
    }
    if ($cpt == count($tabAgences)) { 
        $nomAgence = strtolower(readline ("Veuillez renseigner le nom de l'agence : "));
        while (!(preg_match('#^[a-z ]{1,}$#', $nomAgence))){
            echo "---------> Votre saisie n'est pas valide  ".PHP_EOL;
            echo (PHP_EOL);
            $nomAgence = strtolower(readline ("Veuillez renseigner le nom de l'agence : "));
            
        }
        $adresseAgence = readline ("Veuillez renseigner l'adresse complète (rue, code postal, ville) : ");
        while (!(preg_match('#^[a-z 1-9]{1,}$#', $adresseAgence))){
            echo "---------> Votre saisie n'est pas valide :  ".PHP_EOL;
            echo (PHP_EOL);
            $adresseAgence = readline ("Veuillez renseigner l'adresse, code postal, ville : ");
            
        }
        echo (PHP_EOL);


        $Agence["codeAgence"] = $cpteurAgence;
        $Agence["nomAgence"] = ucfirst($nomAgence);
        $Agence["adresseAgence"] = $adresseAgence; 
    
        $tabAgences [] = $Agence; //entrer le tab agence dans le tab général 
        echo ("~~~~ Votre saisie est enregistée ~~~~ ".PHP_EOL);
		echo (PHP_EOL);
		
		echo ("Code agence :".$Agence["codeAgence"].PHP_EOL);
		echo (PHP_EOL);
		
		echo ("Nom d'agence : ".$Agence["nomAgence"].PHP_EOL);
		echo (PHP_EOL);
		
		echo ("Adresse : ".$Agence["adresseAgence"].PHP_EOL);
        echo (PHP_EOL);
        //var_dump ($tabAgences);
        
    }

}

function affichageClient (&$client){
    echo ("N° identifiant : ".$client["identifiant"].PHP_EOL);
	echo (PHP_EOL);
	
	echo ("Nom : ".$client["nom"].PHP_EOL);
	echo (PHP_EOL);
	
	echo ("prénom : ".$client["prenom"].PHP_EOL);
	echo (PHP_EOL);
	
	echo ("Date de naissance : ".$client["dateNais"].PHP_EOL);
	echo (PHP_EOL);
	
	echo ("E-mail : ".$client["email"].PHP_EOL);
    echo PHP_EOL;
}

function affichageCompte(&$compte, $tabAgences, $codeAgence, $cpteurAgence){
    
    echo ("           - Type : ".$compte["type"].PHP_EOL);
    echo (PHP_EOL);
    
	echo ("           - N° compte : ".$compte["Ncompte"].PHP_EOL);
	echo (PHP_EOL);
	
	echo ("           - Identifiant : ".$compte["identifiant"].PHP_EOL);
	echo (PHP_EOL);
	
	echo ("           - Code agence : " .$compte["codeAgence"].PHP_EOL);
	echo (PHP_EOL);
	
	echo ("           - Nom de l'agence : ".agenceExiste($tabAgences, $codeAgence,$cpteurAgence).PHP_EOL);
	echo (PHP_EOL);
	
	echo ("           - Solde : ".$compte["solde"]." €".PHP_EOL);
	echo (PHP_EOL);
	
	echo ("           - Découvert : ".$compte["decouvert"].PHP_EOL);
    echo PHP_EOL;
    echo ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    echo PHP_EOL;
}

function compteSouhaite($compteexistant, $typeCompte){
    $cpt = 0;
    foreach ($compteexistant as $index) {  // vérif de l'existence du compte

        foreach ($index as $key=>$value) {
        
                if ($key == $typeCompte) {
                    return $cpt+1;
                }
        } 
    } 
}
?>