<?php
// mes variables
$tabAgences = [["codeAgence"=>111, "nomAgence"=> "Banque DWWM", "adresseAgence" => "10 rue de la fortune 59000 lille"]];
$tabCB = [];
$tabClients = [["identifiant"=>"AZ0002", "nom"=> "lou", "prenom" => "lea" , "dateNais" => "11/11/2020", "email" => "lou@gmail.fr" ]];
$cpteurClient = 1;
$cpteurClient = str_pad($cpteurClient, 4, "0", STR_PAD_LEFT);


// mes functions
// function rechercheCompte ($tabCB, $identifiant){
//     $compteexistant = [];
//     $cpt= 0;
//     foreach ($tabCB as $compte) { //parcours de tableau pour trouver les comptes existants
  
//         foreach ($compte as $cle => $valeur) {
       
//             if ($valeur == $identifiant) { 
//                 $cle = strtoupper($compte ["type"]);
//                 $valeur = $compte ["solde"];
//                 $compteexistant []= [$cle=>$valeur];
//                 echo  "Le client détient déja un compte ".$compte ["type"].PHP_EOL ;    
//             } 
//             else {
//                $cpt++;
//             }                        
//         }
//     }
//     return $cpt;

//     if (count ($compteexistant)==3) {
//         echo "Le client détient déja les 3 types de comptes : création impossible".PHP_EOL;
//      }
    
//      else {
//          if ($cpt == 0) {
//              echo "Le client n'a pour l'instant aucun compte de créé".PHP_EOL;
//          }
//     }
// }
// var_dump ();	

echo (PHP_EOL);
echo (PHP_EOL);

echo ("                 *************   Bienvenue à la DWWM_20044 BANQUE!   **********".PHP_EOL);

while (true) {
    
    include ('menu_banque.php');

    if ((preg_match("#^[0-8]+$#", $choix))){  //pour filter les chiffres (sans caractères)  
       
        if ($choix=="1") { //créer une agence

            echo("                    ----- Créer une agence -----" .PHP_EOL);

            $codeAgence = intval(readline ("Veuillez renseigner le code de l'agence (3 chiffres) : "));

            if (preg_match('#^[0-9]{3}$#', $codeAgence)){ 
                $cpt = 0;
                // var_dump ($tabAgences[0]["codeAgence"]);

                for ($i=0; $i<count($tabAgences); $i++) { 
                    if ($tabAgences[$i]["codeAgence"] == $codeAgence) { //a tester
                        echo " ---->> Ce code existe déja !!";
                        break;
                        } 
                    else {
                        $cpt++;
                    }
                }
                if ($cpt == count($tabAgences)) { 
                    $nomAgence = readline ("Veuillez renseigner le nom de l'agence : ");
                    $adresseAgence = readline ("Veuillez renseigner l'adresse : ");
                    echo (PHP_EOL);
                    $Agence["codeAgence"] = $codeAgence;
                    $Agence["nomAgence"] = ucfirst($nomAgence);
                    $Agence["adresseAgence"] = $adresseAgence; 
                
                    $tabAgences [] = $Agence; //entrer le tab agence dans le tab général 
                    echo ("~~~~ Votre saisie est enregistée ~~~~ ").PHP_EOL;
					echo (PHP_EOL);
					echo "Code agence :".$Agence["codeAgence"].PHP_EOL;
					echo (PHP_EOL);
					echo "Nom d'agence : ".$Agence["nomAgence"].PHP_EOL;
					echo (PHP_EOL);
					echo "Adresse : ".$Agence["adresseAgence"].PHP_EOL;
					echo (PHP_EOL);
                    //var_dump ($tabAgences);
                }
            }
            else {
                echo ("---------> Saisie incorrecte <----------");
            }
        
        }
        elseif ($choix=="2") {  // créer un client

            echo("                       ----- Créer un client -----" .PHP_EOL);

           
                $nom = strtolower(readline ("Veuillez renseigner le nom du client : "));
                $prenom = strtolower(readline ("Veuillez renseigner le prénom du client : "));
                $jour =  readline ("Veuillez saisir le jour de naissance du client (jj) : ");
                $mois = readline ("Veuillez saisir le mois de naissance du client (mm) : ");
                $annee = readline ("Veuillez saisir l'annee de naissance du client (aaaa) : ");
                $date = ($jour."/".$mois."/".$annee);
                $email = readline ("Veuillez renseigner l'e-mail du client : ");
            
                if ((filter_var($email, FILTER_VALIDATE_EMAIL))&&
                ((preg_match("#^[a-z]{2,}+$#", $nom)))&&
                ((preg_match("#^[a-z]{2,}+$#", $prenom)))&&
                ((preg_match("#^[0-9]{2}+$#", $jour)))&&
                ((preg_match("#^[0-9]{2}+$#", $mois)))&&
                ((preg_match("#^[0-9]{4}+$#", $annee)))){
            
                $cpt = 0;
            
                    for ($i=0; $i<count($tabClients); $i++) { 
                        if (($tabClients != null)&&($tabClients[$i]["nom"] == $nom)&&($tabClients[$i]["prenom"] == $prenom)&&($tabClients[$i]["dateNais"] == $date)) { //fonctionne pas
                            echo " ---->> Ce client existe déja dans notre base de donnée !!";
                            break;
                            } 
                        else {
                            $cpt++;
                        }
                    }
                   
                    if ($cpt == count($tabClients)) { 
                        echo (PHP_EOL);
                       
                        $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            
                        $client["identifiant"]= ($alphabet[rand(0, strlen($alphabet))].$alphabet[rand(0, strlen($alphabet))].$cpteurClient);
                        $client["nom"] = $nom;
                        $client["prenom"] = $prenom;
                        $client["dateNais"] = $date; 
                        $client["email"] = $email;
                    
                        $tabClients [] = $client; //entrer le tab client dans le tab tabclients 
                        echo ("~~~~ Votre saisie est enregistée ~~~~ ".PHP_EOL."N° identifiant :"
                        .$client["identifiant"].PHP_EOL."Nom : ".$client["nom"].PHP_EOL."prénom : "
                        .$client["prenom"].PHP_EOL."Date de naissance : ".$client["dateNais"].PHP_EOL."E-mail : ".$client["email"].PHP_EOL);
                        
                        // var_dump ($tabClients);
                        $cpteurClient=str_pad($cpteurClient + 1, 4, "0", STR_PAD_LEFT);
                    }
                }
                else {
                    echo ("---------> Une ou plusieurs Saisies incorrectes ou manquantes <----------");
                }
            

        }
        elseif ($choix=="3") { // créer compte bancaire

            echo("                         ----- Créer un compte bancaire -----" .PHP_EOL);

                  
            $numIdentifiant = strtolower(readline ("Avez-vous un identifiant client (o/n) : "));

            if (preg_match("#^[n]+$#", $numIdentifiant)) {
                echo "Veuillez tout d'abord créer un compte client ...".PHP_EOL;
                // fonction a travailler
            }
            if (preg_match("#^[o]+$#", $numIdentifiant)) {
                
                $identifiant = strtoupper(readline("Veuillez saisir l'identifiant du client : "));

                if (preg_match("#^[A-Z]{2}[0-9]{4}+$#", $identifiant)) {
                    $compteexistant = [];
                    $cpt= 0;
                    foreach ($tabCB as $compte) { //parcours de tableau pour trouver les comptes existants
                  
                        foreach ($compte as $cle => $valeur) {
                       
                            if ($valeur == $identifiant) { 
                                $cle = strtoupper($compte ["type"]);
                                $valeur = $compte ["solde"];
                                $compteexistant []= [$cle=>$valeur];
                                echo  "Le client détient déja un compte ".$compte ["type"].PHP_EOL ;    
                            } 
                            else {
                               $cpt++;
                            }                        
                        }
                    }
                                    
                    if (count ($compteexistant)==3) {
                        echo "Le client détient déja les 3 types de comptes : création impossible".PHP_EOL;
                     }
                    
                     else {
                         if ($cpt == 0) {
                             echo "Le client n'a pour l'instant aucun compte de créé".PHP_EOL;
                         }
                    }
                    //var_dump ($tabCB);
                    
                        $typeCompte = strtoupper (readline ("Veuillez renseigner le type de compte souhaité (Livret a => LA, Compte courant => CC ou PEL => PE): "));
                    

                        if (preg_match("#^[LA]+$#", $typeCompte)) {
                            $cpt = 0;
                                                  
                            foreach ($compteexistant as $index) {  // vérif de l'existence du compte
                            
                                foreach ($index as $key=>$value) {
                                
                                        if ($key == $typeCompte) {
                                            $cpt++;      
                                        }
                                } 
                            } 
                            if ($cpt >0) {
                                echo "Saisie invalide, le client détient déja un ".$typeCompte.PHP_EOL;
                            }
                            else {                  
                                $codeAgence =  readline ("Veuillez saisir le code agence : ");
                                
                                if ((preg_match('#^[0-9]{3}$#', $codeAgence))){ //verif code agence
                                             
                                    $solde = floatval (readline ("Veuillez saisir le solde déposé : "));

                                    while (preg_match('#^[\W]{1,}$#', $solde)) {// ne fonctionne pas à revoir
                                        echo "Le solde n'est pas un numérique";
                                        $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
                                    }  
                                    $decouvert = strtolower(readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL));
                                   
                                    while (($decouvert != "o")&&($decouvert !="n")){
                                        echo "Votre saisie n'est pas valide".PHP_EOL;
                                        $decouvert = readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL);
                                    }
                                    $decouvert == "o" ? $decouvert = "oui" : $decouvert = "non";

                                    // insertion des données
                                    $numCompte= date('dmyHis'); //12 au lieu de 11, à voir
                                    $compte["Ncompte"]= $numCompte;
                                    $compte["identifiant"] = strtoupper($identifiant);
                                    $compte["codeAgence"] = $codeAgence;
                                    $compte["type"] = $typeCompte; 
                                    $compte["solde"] = $solde;
                                    $compte["decouvert"] = strtolower($decouvert);
                    
                                    $tabCB[]=$compte;
                                    echo PHP_EOL;
                                    echo ("~~~~ Votre saisie est enregistée ~~~~ ".PHP_EOL."Type de compte : ".$compte["type"].PHP_EOL."N° compte :"
                                    .$compte["Ncompte"].PHP_EOL."Identifiant : ".$compte["identifiant"].PHP_EOL."Code agence : "
                                    .$compte["codeAgence"].PHP_EOL."Nom de l'agence : ".PHP_EOL."Solde : ".$compte["solde"]." €".PHP_EOL."Découvert : ".$compte["decouvert"].PHP_EOL);
                                            //nom agence voir pour function
                                                         
                                }
                                else {
                                    echo ("Code agence erroné ! ");
                                }
                            }
                        }
                        if (preg_match("#^[CC]+$#", $typeCompte)) {
                            $cpt = 0;
                                                  
                            foreach ($compteexistant as $index) {  // vérif de l'existence du compte
                            
                                foreach ($index as $key=>$value) {
                                
                                        if ($key == $typeCompte) {
                                            $cpt++;      
                                        }
                                } 
                            } 
                            if ($cpt >0) {
                                echo "Saisie invalide, le client détient déja un ".$typeCompte.PHP_EOL;
                            }
                            else {                  
                                $codeAgence =  readline ("Veuillez saisir le code agence : ");
                                
                                if ((preg_match('#^[0-9]{3}$#', $codeAgence))){ //verif code agence
                                             
                                    $solde = floatval (readline ("Veuillez saisir le solde déposé : "));

                                    while (preg_match('#^[\W]{1,}$#', $solde)) {// ne fonctionne pas à revoir
                                        echo "Le solde n'est pas un numérique";
                                        $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
                                    }  
                                    $decouvert = strtolower(readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL));
                                   
                                    while (($decouvert != "o")&&($decouvert !="n")){
                                        echo "Votre saisie n'est pas valide".PHP_EOL;
                                        $decouvert = readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL);
                                    }
                                    $decouvert == "o" ? $decouvert = "oui" : $decouvert = "non";

                                    // insertion des données
                                    $numCompte= date('dmyHis'); //12 au lieu de 11, à voir
                                    $compte["Ncompte"]= $numCompte;
                                    $compte["identifiant"] = strtoupper($identifiant);
                                    $compte["codeAgence"] = $codeAgence;
                                    $compte["type"] = $typeCompte; 
                                    $compte["solde"] = $solde;
                                    $compte["decouvert"] = strtolower($decouvert);
                    
                                    $tabCB[]=$compte;
                                    echo PHP_EOL;
                                    echo ("~~~~ Votre saisie est enregistée ~~~~ ".PHP_EOL."Type de compte : ".$compte["type"].PHP_EOL."N° compte :"
                                    .$compte["Ncompte"].PHP_EOL."Identifiant : ".$compte["identifiant"].PHP_EOL."Code agence : "
                                    .$compte["codeAgence"].PHP_EOL."Nom de l'agence : ".PHP_EOL."Solde : ".$compte["solde"]." €".PHP_EOL."Découvert : ".$compte["decouvert"].PHP_EOL);
                                            //nom agence voir pour function
                                                         
                                }
                                else {
                                    echo ("Code agence erroné ! ");
                                }
                            }
                        }
                        if (preg_match("#^[PE]+$#", $typeCompte)) {
                            $cpt = 0;
                                                  
                            foreach ($compteexistant as $index) {  // vérif de l'existence du compte
                            
                                foreach ($index as $key=>$value) {
                                
                                        if ($key == $typeCompte) {
                                            $cpt++;      
                                        }
                                } 
                            } 
                            if ($cpt >0) {
                                echo "Saisie invalide, le client détient déja un ".$typeCompte.PHP_EOL;
                            }
                            else {                  
                                $codeAgence =  readline ("Veuillez saisir le code agence : ");
                                
                                if ((preg_match('#^[0-9]{3}$#', $codeAgence))){ //verif code agence
                                             
                                    $solde = floatval (readline ("Veuillez saisir le solde déposé : "));

                                    while (preg_match('#^[\W]{1,}$#', $solde)) {// ne fonctionne pas à revoir
                                        echo "Le solde n'est pas un numérique";
                                        $solde = floatval (readline ("Veuillez saisir le solde déposé : "));
                                    }  
                                    $decouvert = strtolower(readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL));
                                   
                                    while (($decouvert != "o")&&($decouvert !="n")){
                                        echo "Votre saisie n'est pas valide".PHP_EOL;
                                        $decouvert = readline ("Veuillez préciser si découvert autorisé ou pas (o/n) : ".PHP_EOL);
                                    }
                                    $decouvert == "o" ? $decouvert = "oui" : $decouvert = "non";

                                    // insertion des données
                                    $numCompte= date('dmyHis'); //12 au lieu de 11, à voir
                                    $compte["Ncompte"]= $numCompte;
                                    $compte["identifiant"] = strtoupper($identifiant);
                                    $compte["codeAgence"] = $codeAgence;
                                    $compte["type"] = $typeCompte; 
                                    $compte["solde"] = $solde;
                                    $compte["decouvert"] = strtolower($decouvert);
                    
                                    $tabCB[]=$compte;
                                    echo PHP_EOL;
                                    echo ("~~~~ Votre saisie est enregistée ~~~~ ".PHP_EOL."Type de compte : ".$compte["type"].PHP_EOL."N° compte :"
                                    .$compte["Ncompte"].PHP_EOL."Identifiant : ".$compte["identifiant"].PHP_EOL."Code agence : "
                                    .$compte["codeAgence"].PHP_EOL."Nom de l'agence : ".PHP_EOL."Solde : ".$compte["solde"]." €".PHP_EOL."Découvert : ".$compte["decouvert"].PHP_EOL);
                                            //nom agence voir pour function
                                                         
                                }
                                else {
                                    echo ("Code agence erroné ! ");
                                }
                            }
                        }
                    
                    
                }
                else 
                echo "Identifiant incorrecte";
            }
            else {
                echo "Saisie incorrecte";
            }
        }
         elseif ($choix=="4") {

            echo("                         ----- Recherche de compte -----" .PHP_EOL);
			echo (PHP_EOL);
			
			$recherche_numero_compte = readline(" Veuillez saisir un numéro de compte : " . PHP_EOL);

			$id_personne_chercher = NULL;

			foreach ($tabCB as $num_compte_en_cours) {
	
				if ($num_compte_en_cours["Ncompte"] === "$recherche_numero_compte") {
		
				$id_personne_chercher = $num_compte_en_cours ["identifiant"];
				break;
				
				}	
			}

				if ($id_personne_chercher == NULL) {
				echo ("ce compte n'existe pas");
				
				} 

				else {
		
					foreach ($tabClients as $client_en_cours) {
			
						if ($client_en_cours["identifiant"] === $id_personne_chercher ) {
			
						echo("Client : ".PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Code agence : ".$num_compte_en_cours["codeAgence"].PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Identifiant : ".$client_en_cours["identifiant"].PHP_EOL);
						echo (PHP_EOL);
						
						
						echo(" Nom : ".$client_en_cours["nom"].PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Prénom : ".$client_en_cours["prenom"].PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Solde : ".$num_compte_en_cours["solde"].PHP_EOL);
						echo (PHP_EOL);
						
						echo(" Découvert autorisé: ".$num_compte_en_cours["decouvert"].PHP_EOL);
						echo (PHP_EOL);
           
						}
					}
				}
			
        }   
        elseif ($choix=="5") {

            echo("                         ----- Recherche de client -----" .PHP_EOL);
            echo (PHP_EOL);
			
			while (true) {

				echo (PHP_EOL);
				echo("1) Recherche par nom" .PHP_EOL);
				echo (PHP_EOL);
				
				echo("2) Recherche par numéro de compte" .PHP_EOL);
				echo (PHP_EOL);
	
				echo("3) Recherche par identifiant client" .PHP_EOL);
				echo (PHP_EOL);
	
				echo("4) Retour au menu principal" .PHP_EOL);
				echo (PHP_EOL);
	
	
				$choix_recherche = readline(" Veuillez saisir un mode de recherche : " .PHP_EOL);
				echo (PHP_EOL);		
	
	
	if ($choix_recherche=="1") {
		
		//function recherche($recherche_1, $tabClients, [nom]) {}
		
		echo("                         ----- Recherche par nom -----" .PHP_EOL);
		echo (PHP_EOL);
		
		$recherche_nom = readline(" Veuillez saisir un nom : " . PHP_EOL);
		echo (PHP_EOL);
		

		$cpt_nom=0;
		
		foreach ($tabClients as $client) {
	
				foreach ($client as $key => $value) {
				
				if ($key == "nom" && $recherche_nom === $value) {
						
						
					
					echo ("Identifiant client : ".$client ["identifiant"]).PHP_EOL;
					echo ("Nom : ".$client ["nom"]).PHP_EOL;
					echo ("Prenom : ".$client ["prenom"]).PHP_EOL;
					echo ("Date de naissance : ".$client ["dateNais"]).PHP_EOL;
					echo ("E-mail : ".$client ["email"]).PHP_EOL;
							
					echo (PHP_EOL);
					
				}
				
				elseif ($key == "nom" && $recherche_nom !== $value)  {
						
					$cpt_nom ++;
					
					}
				}	
				
					
		}
					if ($cpt_nom == count ($tabClients))   {
					echo("Aucune correspondance trouvée.");
					echo (PHP_EOL);
					}
	}	
	
	if ($choix_recherche=="2") {
		
		//function recherche($recherche_2, $tabCb, [num_compte]) {}
			
		echo("                         ----- Recherche par n° de compte -----" .PHP_EOL);
		echo (PHP_EOL);
		
		$recherche_numero_compte = readline(" Veuillez saisir un numéro de compte : " . PHP_EOL);

		$id_personne_chercher = NULL;

		foreach ($tabCB as $num_compte_en_cours) {
	
			if ($num_compte_en_cours["Ncompte"] === "$recherche_numero_compte") {
		
        $id_personne_chercher = $num_compte_en_cours ["identifiant"];
        break;
		
			}	
		}

			if ($id_personne_chercher == NULL) {
			echo ("Aucune correspondance trouvée");
	
			} 

			else {
		
				foreach ($tabClients as $client_en_cours) {
			
					if ($client_en_cours["identifiant"] === $id_personne_chercher ) {
			
					echo("Client : ".PHP_EOL);
					echo (PHP_EOL);
			
					echo(" Identifiant :".$client_en_cours["identifiant"].PHP_EOL);
					echo (PHP_EOL);
			
					echo(" Nom :".$client_en_cours["nom"].PHP_EOL);
					echo (PHP_EOL);
					
					echo(" Prénom :".$client_en_cours["prenom"].PHP_EOL);
					echo (PHP_EOL);
					
					echo(" Date de naissance :".$client_en_cours["dateNais"].PHP_EOL);
					echo (PHP_EOL);
					
					echo(" E-mail :".$client_en_cours["email"].PHP_EOL);
					echo (PHP_EOL);
				   
					}
			}
			}
	}		
	
	if ($choix_recherche=="3") {
	
		//function recherche($recherche_3, $tabCb, [id_client]) {}
			
		echo("                         ----- Recherche par identifiant client -----" .PHP_EOL);
		echo (PHP_EOL);
				
			$recherche_identifiant = readline(" Veuillez saisir un identifiant client : " . PHP_EOL);
		echo (PHP_EOL);
		
		$cpt_id=0;
		
		foreach ($tabClients as $client) {
	
				foreach ($client as $key => $value) {
				
					if ($key == "identifiant" && $recherche_identifiant == $value) {
										
					echo ("Identifiant client : ".$client ["identifiant"]).PHP_EOL;
					echo ("Nom : ".$client ["nom"]).PHP_EOL;
					echo ("Prenom : ".$client ["prenom"]).PHP_EOL;
					echo ("Date de naissance : ".$client ["dateNais"]).PHP_EOL;
					echo ("E-mail : ".$client ["email"]).PHP_EOL;
							
					echo (PHP_EOL);
					}
					
					elseif ($key == "identifiant" && $recherche_identifiant !== $value)  {
						
					$cpt_id++ ;
					
					}
				}	
					
		}
					if ($cpt_id == count ($tabClients))   {
					echo("Aucune correspondance trouvée.");
					echo (PHP_EOL);
					}
	}
	
	if ($choix_recherche=="4") {
	
		echo("                         ----- Retour au menu principal -----" .PHP_EOL);
		echo (PHP_EOL);
			
			$retour= readline("Confirmez (o/n) : " .PHP_EOL);
		
				echo (PHP_EOL);
		
			if ($retour=="o") {
												
				break;
			}
	}	
		}
}
        elseif ($choix=="6") { //liste des comptes d'un client

            echo("                         ----- Afficher la liste des comptes d’un client -----" .PHP_EOL);

            $identifiant = strtoupper(readline("Veuillez saisir l'identifiant du client pour la consultation : "));

            if (preg_match("#^[A-Z]{2}[0-9]{4}+$#", $identifiant)) {
              
                
                foreach ($tabClients as $client) { //parcours de tableau pour trouver les comptes existants
              
                    foreach ($client as $cle => $valeur) {

                        if ($valeur == $identifiant) {
                                           
                            echo ("~~~~ Liste des comptes du client ".$client["prenom"]." ".$client["nom"]." ~~~~ ".PHP_EOL."N° identifiant :"
                            .$client["identifiant"].PHP_EOL."Nom : ".$client["nom"].PHP_EOL."prénom : "
                            .$client["prenom"].PHP_EOL."Date de naissance : ".$client["dateNais"].PHP_EOL."E-mail : ".$client["email"].PHP_EOL);
                        }
                    }
                }
                                
            }
            $cpt= 0;
 
                foreach ($tabCB as $compte) {
                    foreach ($compte as $key => $value) {
                        if ($value == $identifiant){
                            echo ("~~~~~~~~~~".PHP_EOL."Type de compte : ".$compte ["type"].PHP_EOL.$key."  :  ".$value.PHP_EOL);
                            echo ("Solde : ".$compte["solde"]." €".PHP_EOL."Decouvert autorisé : ".$compte["decouvert"]);
                            echo (PHP_EOL);
                        }
                        else{
                           $cpt ++;
                        }           
                    }
                   
                }
            
                if ($cpt == 0 ) {
                    echo "Le client n'a pour l'instant aucun compte de créé".PHP_EOL;
                }

            
                
                                  
        }
        elseif ($choix=="7") {

            echo("                         ----- Imprimer les infos client -----" .PHP_EOL);
        }
        elseif ($choix=="8")  {

            //         echo("                         ----- Quitter -----" .PHP_EOL);
            //         echo (PHP_EOL);
            
            // $quitter= readline("Confirmez (o/n) : " .PHP_EOL);
            
            //         echo (PHP_EOL);
            
            //     if ($quitter=="o") {
                    
            //         echo ("A bientôt!".PHP_EOL);
                    
                    break;
                // }		
        }
    }
    else {
        echo "  :(   Mmmhh, ... Saisie invalide   :(  ".PHP_EOL;
    }
}

?>